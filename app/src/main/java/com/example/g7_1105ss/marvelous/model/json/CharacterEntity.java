package com.example.g7_1105ss.marvelous.model.json;

import com.google.gson.annotations.SerializedName;

import java.util.Date;


public class CharacterEntity {

    private @SerializedName("id") int id;
    private @SerializedName("name") String name;
    private @SerializedName("description") String description;
    private @SerializedName("modified") Date modified;
    private @SerializedName("thumbnail") CharacterThumbnailEntity thumbnail;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public CharacterThumbnailEntity getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(CharacterThumbnailEntity thumbnail) {
        this.thumbnail = thumbnail;
    }
}
