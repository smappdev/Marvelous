package com.example.g7_1105ss.marvelous.model;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Character extends RealmObject{

    private String id;
    @PrimaryKey private String name;
    private String description;
    private String thumbnail;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
