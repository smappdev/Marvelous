package com.example.g7_1105ss.marvelous.views;

import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.g7_1105ss.marvelous.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;


public class CharacterListRowAdapter extends RecyclerView.ViewHolder {

    private String name;
    private String description;
    private String thumbnail;

    private TextView nameTextView;
    private TextView descriptionTextView;
    private CircleImageView thumbnailCircleImageView;
    private ScrollView scrollView;

    public CharacterListRowAdapter(View itemView) {
        super(itemView);

        nameTextView = (TextView) itemView.findViewById(R.id.row_character___name);
        descriptionTextView = (TextView) itemView.findViewById(R.id.row_character___description);
        thumbnailCircleImageView = (CircleImageView) itemView.findViewById(R.id.row_character___thumbnail);
        scrollView = (ScrollView) itemView.findViewById(R.id.row_character___scrollview_description);


        scrollView.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.nameTextView.setText(name);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.descriptionTextView.setText(description);
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        Picasso.with(itemView.getContext()).load(thumbnail).into(thumbnailCircleImageView);
        this.thumbnail = thumbnail;
    }
}
