package com.example.g7_1105ss.marvelous.model.json;

import com.google.gson.annotations.SerializedName;


public class CharacterResponseEntity {

    private @SerializedName("code") int responseCode;
    private @SerializedName("status") String status;
    private @SerializedName("attributionText") String attributionText;
    private @SerializedName("data") CharacterDataResponseEntity data;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAttributionText() {
        return attributionText;
    }

    public void setAttributionText(String attributionText) {
        this.attributionText = attributionText;
    }

    public CharacterDataResponseEntity getData() {
        return data;
    }

    public void setData(CharacterDataResponseEntity data) {
        this.data = data;
    }
}
