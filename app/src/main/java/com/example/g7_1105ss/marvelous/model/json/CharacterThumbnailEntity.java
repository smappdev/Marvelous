package com.example.g7_1105ss.marvelous.model.json;

import com.google.gson.annotations.SerializedName;

public class CharacterThumbnailEntity {

    private @SerializedName("path") String path;
    private @SerializedName("extension") String extension;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
}
