package com.example.g7_1105ss.marvelous.model.entities;

import com.example.g7_1105ss.marvelous.model.Character;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CharacterDataEntity {

    @SerializedName("total") private int total;
    @SerializedName("results") private List<CharacterResultsEntity> results;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<CharacterResultsEntity> getResults() {
        return results;
    }

    public void setResults(List<CharacterResultsEntity> results) {
        this.results = results;
    }
}
