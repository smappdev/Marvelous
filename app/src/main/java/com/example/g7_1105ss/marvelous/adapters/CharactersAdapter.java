package com.example.g7_1105ss.marvelous.adapters;


import android.content.Context;
import android.support.v7.widget.ContentFrameLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.g7_1105ss.marvelous.R;
import com.example.g7_1105ss.marvelous.model.Character;
import com.example.g7_1105ss.marvelous.model.CharacterList;
import com.example.g7_1105ss.marvelous.views.CharacterListRowAdapter;

public class CharactersAdapter extends RecyclerView.Adapter<CharacterListRowAdapter>{

    private CharacterList characterList;
    private LayoutInflater layoutInflater;

    public CharactersAdapter(CharacterList characterList, Context context) {
        this.characterList = characterList;
        layoutInflater = layoutInflater.from(context);
    }

    @Override
    public CharacterListRowAdapter onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.row_character, parent, false);
        return new CharacterListRowAdapter(view);
    }

    @Override
    public void onBindViewHolder(CharacterListRowAdapter holder, int position) {
        Character character = characterList.getCharacters().get(position);

        holder.setName(character.getName());
        holder.setDescription(character.getDescription());
        holder.setThumbnail(character.getThumbnail());

    }

    @Override
    public int getItemCount() {
        return characterList.getCharacters().size();
    }
}
